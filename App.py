
"""
---------- Dedicated to Mary our mother in spirit, and to her son Jesus our brother in spirit.
---------- Just like Adam and Eve. A new beginning...
"""

import sys
sys.path.append('./libs')
import xml2gui as g


if __name__ == '__main__':
    xml = open('app.xml', mode='r', encoding='utf-8').read()
    g.mcxml_loop(xml)
    
